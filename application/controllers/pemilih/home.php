<?php
defined('BASEPATH') OR exit ('No direct script access alowed');

class Home extends CI_Controller{

	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') != "login"){
			redirect(base_url("pemilih/login"));
		}
	}
	
	public function index(){
		$this->load->model('pemilih/m_voting');
		$data['calon'] = $this->m_voting->getCalon();
		$this->load->view('pemilih/v_home', $data);
	}

}
?>