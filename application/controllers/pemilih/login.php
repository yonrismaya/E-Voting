<?php 

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('pemilih/m_login');

	}

	function index(){
		$this->load->view('pemilih/v_login');
	}

	function aksi_login(){
		$username = $this->input->post('login');
		$password = $this->input->post('password');
		$where = array(
			'login' => $username,
			'password' => $password
			);
		$cek = $this->m_login->cek_login("pemilih",$where)->num_rows();
		if($cek > 0){

			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);

			$this->session->set_userdata($data_session);

			redirect(base_url("index.php/pemilih/home"));

		}else{
			echo "Username dan password salah !";
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('index.php/pemilih/login'));
	}
}