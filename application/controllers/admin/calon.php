<?php
defined('BASEPATH') OR exit ('No direct script access alowed');

class Calon extends CI_Controller{

	function __construct(){
      parent::__construct();
      $this->load->model('admin/m_calon');
      	$this->load->helper(array('form', 'url'));
   	}
	
	function index(){
		$data['calon'] = $this->m_calon->getCalon();
		$this->load->view('admin/v_calon', $data);
	}

	function tambah_calon(){
		$this->load->view('admin/v_calon_add');
	}

	function proses_insert(){
		$config['upload_path']         = 'images/';  // folder upload 
        $config['allowed_types']        = 'gif|jpg|png'; // jenis file
        $config['max_size']             = 3000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

		$this->load->library('upload', $config);
		
		if(!$this->upload->do_upload('gambar')){
			echo 'Data Gagal Disimpan';
		}else{
			//tampung data dari form
			$nama_kepala = $this->input->post('nama_kepala');
			$nama_wakil = $this->input->post('nama_wakil');
			$visi = $this->input->post('visi');
			$misi = $this->input->post('misi');
			$file = $this->upload->data();
			$photo = $file['file_name'];

			$this->m_calon->add_calon(array(
				'nama_kepala' => $nama_kepala,
				'nama_wakil' => $nama_wakil,
				'visi' => $visi,
				'misi' => $misi,
				'photo' => $photo
				));
			redirect('admin/calon/index');
		}
	}

	function delete($calon_id){
        $this->m_calon->delete($calon_id);
        redirect('admin/calon/index');
	}	

	function edit($calon_id){
		$data['calon'] = $this->m_calon->edit_data('calon');
		$this->load->view('admin/v_calon_edit',$data);
	}

	function proses_edit(){
		$calon_id = $this->input->post('calon_id');
		$nama_kepala = $this->input->post('nama_kepala');
		$nama_wakil = $this->input->post('nama_wakil');
		$visi = $this->input->post('visi');
		$misi = $this->input->post('misi');
		$photo = $this->input->post('photo');
	 
		$data = array(
			'nama_kepala' => $nama_kepala,
			'nama_wakil' => $nama_wakil,
			'visi' => $visi,
			'misi' => $misi,
			'photo' => $photo
		);
	 
		$where = array(
			'calon_id' => $calon_id
		);
	 
		$this->m_calon->update_data($where,$data,'calon');
		redirect('calon/index');
	}

}
?>