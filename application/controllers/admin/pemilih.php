<?php
defined('BASEPATH') OR exit ('No direct script access alowed');

class Pemilih extends CI_Controller{

	function __construct(){
      parent::__construct();
      $this->load->model('admin/m_pemilih');
      	$this->load->helper(array('form', 'url'));
   	}
	
	public function index(){
		$data['pemilih'] = $this->m_pemilih->getPemilih();
		$this->load->view('admin/v_pemilih', $data);
	}
	function tambah_pemilih(){
		$this->load->view('admin/v_pemilih_add');
	}

	function proses_insert(){
		$login = $this->input->post('login');
		$password=$this->input->post('password');
		$nama=$this->input->post('nama');
		$alamat=$this->input->post('alamat');
		$jk=$this->input->post('jk');
		$tgl_lahir=$this->input->post('tgl_lahir');
		$umur=$this->input->post('umur');
		$status=$this->input->post('status');
		$telepon=$this->input->post('telepon');
		$data = array(  
			'login'=>$login, 
			'password'=>$password,
			'nama'=>$nama,
			'alamat'=>$alamat,
			'jk'=>$jk,
			'tgl_lahir'=>$tgl_lahir,
			'umur'=>$umur,
			'status'=>$status,
			'telepon'=>$telepon,
			 );
		$this->db->insert('pemilih',$data);
		redirect('admin/pemilih/index');
    }

	public function importPemilih(){

		include APPPATH.'libraries\PHPExcel.php';
    
	    $excelreader = new PHPExcel_Reader_Excel2007();
	    $loadexcel = $excelreader->load('assets/tmp/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
	    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
    
	    // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
	    $data = [];
    
	    $numrow = 1;
	    foreach($sheet as $row){
	      // Cek $numrow apakah lebih dari 1
	      // Artinya karena baris pertama adalah nama-nama kolom
	      // Jadi dilewat saja, tidak usah diimport
	      if($numrow > 1){
	        // Kita push (add) array data ke variabel data
	        array_push($data, [
	          'login'=>$row['A'], // Insert data nis dari kolom A di excel
	          'password'=>$row['B'], // Insert data nama dari kolom B di excel
	          'nama'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
	          'alamat'=>$row['D'], // Insert data alamat dari kolom D di excel
	          'jk'=>$row['E'], // Insert data alamat dari kolom D di excel
	          'tgl_lahir'=>$row['F'], // Insert data alamat dari kolom D di excel
	          'umur'=>$row['G'], // Insert data alamat dari kolom D di excel
	          'status'=>$row['H'], // Insert data alamat dari kolom D di excel
	          'telepon'=>$row['I'], // Insert data alamat dari kolom D di excel
	        ]);
	      }
	      
	      $numrow++; // Tambah 1 setiap kali looping
	    }
	    // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
	    $this->m_pemilih->insert_multiple($data);
	    
	    redirect("admin/index"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}
}
?>