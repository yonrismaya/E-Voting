<?php
defined('BASEPATH') OR exit ('No direct script access alowed');

class Jadwal extends CI_Controller{

	function __construct(){
      parent::__construct();
      $this->load->model('admin/m_jadwal');
   	}
	
	public function index(){
		$data['pilkades'] = $this->m_jadwal->getJadwal();
		$this->load->view('admin/v_jadwal', $data);
	}

	function tambah_jadwal(){
		$this->load->view('admin/v_jadwal_add');
	}

}
?>