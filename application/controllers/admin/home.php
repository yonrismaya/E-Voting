<?php
defined('BASEPATH') OR exit ('No direct script access alowed');

class Home extends CI_Controller{

	function __construct(){
      parent::__construct();
      $this->load->model('admin/hasil');
   	}
	
	public function index(){
		$jumlah = $this->hasil->jumlah();
		$sudah = $this->hasil->sudah();
		$belum = $this->hasil->belum();
		$this->load->view('admin/v_home', compact('jumlah','sudah','belum'));
	}

}
?>