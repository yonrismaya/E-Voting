<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
  $this->load->view('pemilih/header'); 
?>
  <?php 
$no    = 1;
?>
<?php foreach ($calon as $row): ?>
  <section>
  <div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-default">
          <center>
            <div class="panel-body">
              <img src="<?php echo base_url() . 'images/' . $row['photo']; ?>" class="img-responsive" alt="">
              <b><?php echo $no++.". ".$row['nama_kepala']; ?></b><p class="text-mutted"><?=$row['nama_wakil'];?></p>
            </div>
            <div class="panel-footer">
              <a data-toggle="modal" href="#detail<?=$row['calon_id']; ?>" class="btn btn-info btn-sm">Detail</a>
              <?php echo '<a href="'.base_url().'index.php/admin/calon/edit/'.$row['calon_id'].'" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-ok"></span> Pilih</a>'?>
            </div>
          </center>
        </div>
      </div>
    </div>
  </section>
<?php endforeach; ?>
<?php $this->load->view('pemilih/footer'); ?>