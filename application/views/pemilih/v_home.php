<?php $this->load->view('pemilih/header'); ?>

    <!-- Main content -->
    <section class="content" style="color: black;">
      <!-- Small boxes (Stat box) -->
    
      <?php 
$no    = 1;
?>
<?php foreach ($calon as $row): ?>
  <section>
  <div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-default">
          <center>
            <div class="panel-body">
              <img src="<?php echo base_url() . 'images/' . $row['photo']; ?>" class="img-responsive" alt="">
              <b>Pasangan Calon No. <?php echo $no++ ?></b>
               <h3 align="center">10<sup style="font-size: 20px">%</sup> Suara</h3>
            </div>
          </center>
        </div>
      </div>
    </div>
  </section>
<?php endforeach; ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php $this->load->view('pemilih/footer'); ?>