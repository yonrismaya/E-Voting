  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.12
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url();?>assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
   
  //Import file excel
  window.setTimeout(function() {
      $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
          $(this).remove(); 
      });
  }, 5000);
   
  });
  </script>
  <script>
    try {
    setTimeout(function(){
      console.clear();
      console.log("%cWelcome!", "color: #000; font-size:45px; font-weight: bold; font-family: Arial");
      console.log("%cAda perlu sesuatu di console?", "color: blue; font-size:25px; font-weight: bold; font-family: Arial");
      console.log("%cJangan paste disini, code yang tidak Anda mengerti.", "color: red; font-size:20px; font-weight: bold; font-family: Arial");
    }, 1000);
    } catch(e) {

    }
  </script>
  <script> 
    //easeScroll
    $("html").easeScroll();

    //Import
    $(document).ready(function(){
      // Sembunyikan alert validasi kosong
      $("#kosong").hide();
    });

    function del(){
      swal({
          title: "Are you sure?",
        text: "Data akan di hapus",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      }, function(){ window.location='?p=del_data'; });
    }

    //Table
    $(function() { $('#tabel').dataTable(); });

    // Preview 1
    var loadFile1 = function(event) {
      var reader = new FileReader();
        reader.onload = function(){
          var output1 = document.getElementById('output1');
          output1.src = reader.result;
        };
      reader.readAsDataURL(event.target.files[0]);
    };

    // Preview 2
    var loadFile2 = function(event) {
      var reader = new FileReader();
        reader.onload = function(){
          var output2 = document.getElementById('output2');
          output2.src = reader.result;
        };
      reader.readAsDataURL(event.target.files[0]);
    };

    //Tambah calon
    tinymce.init({
      selector:"#visi",
      menubar: false,
        plugins: ["preview wordcount advlist autolink lists link image charmap preview pagebreak searchreplace insertdatetime, fullscreen hr , table,  directionality, emoticons, textcolor, colorpicker, textpattern, code"],
        toolbar: "undo redo | fontselect fontsizeselect | styleselect | bullist numlist | forecolor backcolor emoticons | preview wordcount",
        convert_urls: false,
        theme_advanced_font_sizes : "8px,10px,12px,14px,16px,18px,20px,24px,32px,36px",
    });

    tinymce.init({
      selector:"#misi",
      menubar: false,
        plugins: ["preview wordcount advlist autolink lists link image charmap preview pagebreak searchreplace insertdatetime, fullscreen hr , table,  directionality, emoticons, textcolor, colorpicker, textpattern, code"],
        toolbar: "undo redo | fontselect fontsizeselect | styleselect | bullist numlist | forecolor backcolor emoticons | preview wordcount",
        convert_urls: false,
        theme_advanced_font_sizes : "8px,10px,12px,14px,16px,18px,20px,24px,32px,36px",
    });

    //Style
    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
            $(this).find('em:first').toggleClass("glyphicon-minus");      
        }); 
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
      if ($(window).width() > 768) $('#collapse').collapse('show')
    })
    $(window).on('resize', function () {
      if ($(window).width() <= 767) $('#collapse').collapse('hide')
    })
  </script> 

  <!-- collumn chart -->
  <script>
    var chart = echarts.init(document.getElementById('chart'));
    window.onresize = chart.resize
    option = 
    {
        tooltip: { trigger: 'item'},
        toolbox: { show: true, feature: { saveAsImage: { show: true } } },

        calculable: true,
        grid: { borderWidth: 0, y: 35, y2: 35, x: 30, x2: 30 },
        xAxis: [  
            { type: 'category', show: true,
                <?php $hasil = laporan(); ?>
                data: [ <?php foreach($hasil as $row): echo "'".$row['nama']."',"; endforeach; ?> ]
            }
        ],

        yAxis: [ { type: 'value', show: true } ],
        series: [
            { name: '', type: 'bar', 
              itemStyle: {
                  normal: {
                        color: function (params) {
                            // build a color map as your need.
                            var colorList = [
                              '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', 
                              '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d' 
                            ];
                            return colorList[params.dataIndex]
                        },
                        label: { show: true, position: 'top', formatter: '{b}\n{c}' }
                    }
                },
                data: [ <?php foreach($hasil as $row): echo "'".$row['hasil']."',"; endforeach; ?> ],
            }
        ]
    };

    chart.setOption(option);
  </script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
