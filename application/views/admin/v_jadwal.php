<?php $this->load->view('admin/header'); ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Jadwal Pemilihan 
      </h1>
    </section>
	
	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">

			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="panel-heading">
						<a class="btn btn-sm btn-primary" href="<?php echo base_url()."index.php/admin/jadwal/tambah_jadwal"?>">
						<span class="glyphicon glyphicon-upload"></span>
						Tambah Jadwal
						</a>&nbsp;&nbsp;
					</div>
					<div class="panel-body">
						<table id="example1" class="table table-bordered table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Tahun</th>
									<th>Tanggal Mulai</th>
									<th>Tanggal Selesai</th>
									<th>Keterangan</th>
									<th>#</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($pilkades as $row): ?>
								<tr>
									<td><?= $row['pilkades_tahun'] ?></td>
									<td><?= $row['pilkades_tanggal_mulai'] ?></td>
									<td><?= $row['pilkades_tanngal_selesai'] ?></td>
									<td><?= $row['pilkades_keterangan'] ?></td>
									<td>	
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->

	</div>
<?php $this->load->view('admin/footer'); ?>