<?php $this->load->view('admin/header'); ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Pemilih  <a href="<?php echo base_url()."index.php/admin/pemilih/tambah_pemilih"?>" class="btn btn-info pull-right">Tambah Calon</a>
      </h1>
    </section>
	
	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">

			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-info">
					<div class="panel-heading">Data Pemilih&nbsp;&nbsp;
						<a class="btn btn-sm btn-primary" data-toggle="collapse" href="#import" aria-expanded="false" aria-controls="collapseExample">
						<span class="glyphicon glyphicon-upload"></span>
						Import Data
						</a>&nbsp;&nbsp;
					</div>
					<div class="panel-body">
						<div class="collapse" id="import">
						<div class="well">
							<form action="<?php echo base_url('index.php/admin/pemilih/importPemilih')?>" method="post" enctype="multipart/form-data">
								<a href="<?= base_url('assets/file/format_import.xlsx')?>" class="btn btn-default"><span class="glyphicon glyphicon-download"></span>
								Download Format</a><br><br>
								<input type="file" name="file" id="file" style="width: 30%;" class="pull-left form-control">&nbsp;
								<button type="submit" class="btn btn-primary" name="import">
									<span class="glyphicon glyphicon-upload"></span> Import
								</button>
							</form>
						</div>
					</div>
						<table id="example1" class="table table-bordered table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>NIK</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>JK</th>
									<th>STATUS</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($pemilih as $row): ?>
								<tr>
									<td><?= $row['login'] ?></td>
									<td><?= $row['nama'] ?></td>
									<td><?= $row['alamat'] ?></td>
									<td><?= $row['jk'] ?></td>
									<td>
									<?php 
										if($row['status'] == 1) 
										echo "<span class='label label-danger'>Belum Memilih</span></span>";
										// Belum Memilih <span class='glyphicon glyphicon-remove' aria-hidden='true'></span>
										else echo "<span class='label label-success'>Sudah Memilih</span>";
										// Sudah Memilih <span class='glyphicon glyphicon-ok' aria-hidden='true'></span>
									?> 	
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->

	</div>
<?php $this->load->view('admin/footer'); ?>