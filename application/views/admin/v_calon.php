<?php $this->load->view('admin/header'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Calon Kepala Desa <a href="<?php echo base_url()."index.php/admin/calon/tambah_calon"?>" class="btn btn-info pull-right">Tambah Calon</a>   
      </h1>
    </section>
    </br>
<?php 
$no    = 1;
?>
<?php foreach ($calon as $row): ?>
  <section>
  <div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-default">
          <center>
            <div class="panel-body">
              <img src="<?php echo base_url() . 'images/' . $row['photo']; ?>" class="img-responsive" alt="">
              <b><?php echo $no++.". ".$row['nama_kepala']; ?></b><p class="text-mutted"><?=$row['nama_wakil'];?></p>
            </div>
            <div class="panel-footer">
              <a data-toggle="modal" href="#detail<?=$row['calon_id']; ?>" class="btn btn-info btn-sm">Detail</a>
              <?php echo '<a href="'.base_url().'index.php/admin/calon/edit/'.$row['calon_id'].'" class="btn btn-primary btn-sm">Edit</a>'?>
              <?php echo '<a href="'.base_url().'index.php/admin/calon/delete/'.$row['calon_id'].'" class="btn btn-danger btn-sm">Hapus</a>'?>
            </div>
          </center>
        </div>
      </div>
    </div>
  </section>
<?php endforeach; ?>
  </div>

<?php foreach($calon as $row): ?> 
  <!-- Detail -->
  <div class="modal fade" id="detail<?=$row['calon_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Visi dan Misi</h4>
        </div>
        <div class="modal-body">
          <table>
        <tr>
          <td>Nama Kepala Desa</td>
          <td>&nbsp;</td>
          <td>:</td>
          <td>&nbsp;</td>
          <td><?=$row['nama_kepala']; ?></td>
        </tr>
        <tr>
          <td>Nama Wakil Kepala Desa</td>
          <td>&nbsp;</td>
          <td>:</td>
          <td>&nbsp;</td>
          <td><?=$row['nama_wakil']; ?></td>
        </tr>
      </table><br>
          <b>VISI :</b>
            <div><?=$row['visi']; ?></div>
          <b>MISI :</b>
            <div><?=$row['misi']; ?></div>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
<?php $this->load->view('admin/footer'); ?>
