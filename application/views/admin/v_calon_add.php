<?php $this->load->view('admin/header'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Calon Kepala Desa   
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="box box-info">
	<div class="form-horizontal">
		<div class="box-body">
			<!-- form start -->
			<?php echo form_open_multipart('admin/calon/proses_insert'); ?>
			<div class="form-group">
				<div class="col-sm-12">
					<hr>
					<h4 class="box-title" align="center"><b>Form Data Calon Kepala Desa. Tanda (<span style="color: red">*</span>) Wajib diisi</b></h4>
					<hr>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Kepala Desa <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="nama_kepala">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Nama Wakil Kepala Desa <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="nama_wakil">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Visi <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<textarea type="text" class="form-control" name="visi"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Misi <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<textarea type="text" class="form-control" name="misi"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Photo <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="file" class="form-control" name="gambar" onchange="loadFile1(event)">
				</div>
			</div>
			<div class="box-footer">
				<a href="<?= base_url('index.php/admin/calon/index') ?>">
				<div class="btn btn-default">
					Batal
				</div></a>
				<input type="submit" class="btn btn-info pull-right" value="Simpan">
			</div>
			<?php echo form_close() ?>
		</div>
	</div>
	</div>
    </section>
  </div>
<?php $this->load->view('admin/footer'); ?>
