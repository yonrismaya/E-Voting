<?php $this->load->view('admin/header'); ?>
<?php $p = 'pilkada' ?>
<?php $s = 'belum' ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Pemilih  
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="box box-info">
	<div class="form-horizontal">
		<div class="box-body">
			<!-- form start -->
			<?php echo form_open_multipart('admin/pemilih/proses_insert'); ?>
			<div class="form-group">
				<div class="col-sm-12">
					<hr>
					<h4 class="box-title" align="center"><b>Form Data Pemilih. Tanda (<span style="color: red">*</span>) Wajib diisi</b></h4>
					<hr>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">NIK <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="login">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Password <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="password" value="<?php echo $p; ?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">nama <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="nama">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Alamat <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<textarea type="text" class="form-control" name="alamat"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Jenis Kelamin <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="jk">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Lahir <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="tgl_lahir">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Umur <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="umur">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Status <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="status" value="<?php echo $s; ?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Telepon <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="telepon">
				</div>
			</div>
			<div class="box-footer">
				<a href="<?= base_url('index.php/admin/calon/index') ?>">
				<div class="btn btn-default">
					Batal
				</div></a>
				<input type="submit" class="btn btn-info pull-right" value="Simpan">
			</div>
			<?php echo form_close() ?>
		</div>
	</div>
	</div>
    </section>
  </div>
<?php $this->load->view('admin/footer'); ?>
