<?php $this->load->view('admin/header'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
     <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>

                    <?php

                    if(($sudah && $jumlah) > 0){
                      $hasil = ($sudah/$jumlah) * 100;
                      echo ($hasil).'<sup style="font-size: 20px">%</sup>';
                    }else{
                      echo "0%";
                    }
                    ?> </h3>
              <p>Suara Masuk</p>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-volume-up"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $jumlah ?></h3>

              <p>Jumlah Pemilih</p>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-list-alt"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $sudah ?></h3>

              <p>Sudah Memilih</p>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-ok-circle"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $belum ?></h3>

              <p>Belum Memilih</p>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-remove-circle"></i>
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('admin/footer'); ?>
