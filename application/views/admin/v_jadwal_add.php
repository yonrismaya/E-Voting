<?php $this->load->view('admin/header'); ?>
<?php $tahunini = date('Y'); ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Jadwal   
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="box box-info">
	<div class="form-horizontal">
		<div class="box-body">
			<!-- form start -->
			<?php echo form_open_multipart('admin/calon/proses_insert'); ?>
			<div class="form-group">
				<div class="col-sm-12">
					<hr>
					<h4 class="box-title" align="center"><b>Form Tambah Jadwal. Tanda (<span style="color: red">*</span>) Wajib diisi</b></h4>
					<hr>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Tahun Pemilihan <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="pilkades_tahun" value="<?php echo $tahunini;?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Mulai <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="pilkades_tanggal_mulai" id="datepickers">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Selesai <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="pilkades_tanggal_selesai" id="datepickers">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Keterangan <span style="color: red">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" name="pilkades_keterangan">
				</div>
			</div>
			<div class="box-footer">
				<a href="<?= base_url('index.php/admin/jadwal/index') ?>">
				<div class="btn btn-default">
					Batal
				</div></a>
				<input type="submit" class="btn btn-info pull-right" value="Simpan">
			</div>
			<?php echo form_close() ?>
		</div>
	</div>
	</div>
    </section>
  </div>
<?php $this->load->view('admin/footer'); ?>
