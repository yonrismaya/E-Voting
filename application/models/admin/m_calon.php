<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Calon extends CI_Model {

	function getCalon(){
		$this -> db -> select("*");
		$this -> db -> from("calon");
		return $this -> db -> get() -> result_array();
	}

	function add_calon($data){
		$this->db->insert('calon',$data);
	}

	function delete($calon_id){
        $this->db->delete('calon', array('calon_id'=>$calon_id));
    }

	function edit_data($calon_id){		
		$q="SELECT * FROM  calon WHERE calon_id='$calon_id'";
        $query=$this->db->query($q);
        return $query->row();
	}

	function update_data($calon_id, $nama_kepala, $nama_wakil, $visi, $misi, $photo){
		$data = array(
			'calon_id' => $calon_id,
			'nama_kepala' => $nama_kepala,
			'nama_wakil' => $nama_wakil,
			'visi' => $visi,
			'misi' => $misi,
			'photo' => $photo
		);
		$this->db->where('calon_id', $calon_id);
		$this->db->update('calon',$data);
	}

}