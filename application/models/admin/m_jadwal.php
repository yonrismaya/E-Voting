<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Jadwal extends CI_Model {

	function getJadwal(){
		$this->db->select("*");
		$this->db->from("pilkades");
		return $this->db->get()->result_array();
	}
}