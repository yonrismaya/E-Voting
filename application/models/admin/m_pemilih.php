<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Pemilih extends CI_Model {

	function getPemilih(){
		$this -> db -> select("*");
		$this -> db -> from("pemilih");
		return $this -> db -> get() -> result_array();
	}

	function import($filename){
		$this->db->library('upload');

		$config['upload_path'] = 'assets/tmp/';
	    $config['allowed_types'] = 'xlsx';
	    $config['max_size']  = '2048';
	    $config['overwrite'] = true;
	    $config['file_name'] = $filename;

	    $this->upload->initialize($config);
	    if($this->upload->do_upload('file')){
	    	$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	    	return $return;
	    }else{
	    	$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
	    	return $return;
	    }
	}

	function insert_multiple($data){
		$this->db->insert_batch('pemilih', $data);
	}

}