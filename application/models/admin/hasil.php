<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil extends CI_Model {

	function jumlah(){
		$query = $this->db->query("select * from pemilih");
		$jumlah = $query->num_rows();
		return $jumlah;
	}

	function sudah(){
		$query = $this->db->query("select * from pemilih where status='sudah'");
		$sudah = $query->num_rows();
		return $sudah;
	}
	function belum(){
		$query = $this->db->query("select * from pemilih where status='belum'");
		$belum = $query->num_rows();
		return $belum;
	}
}