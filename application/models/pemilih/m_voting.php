<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class M_Voting extends CI_Model {

	function getCalon(){
		$this -> db -> select("*");
		$this -> db -> from("calon");
		return $this -> db -> get() -> result_array();
	}

	public function hasil_voting($pemilih_id, $data){
		$this->db->insert('vote', $data);
		if($this->db->affected_rows()>0){
			$this->db->set('status', 'sudah');
			$this->db->where('pemilih_id', $pemilih_id);
			$this->db->update('pemilih_id');
			if ($this->db->affected_rows()>0) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}


}