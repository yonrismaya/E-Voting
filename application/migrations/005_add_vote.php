<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_vote extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'pemilih_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'calon_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                ));
                $this->dbforge->create_table('vote');
        }

        public function down()
        {
                $this->dbforge->drop_table('vote');
        }
}