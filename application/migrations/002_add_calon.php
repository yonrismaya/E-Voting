<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_calon extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'calon_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'nama_kepala' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'nama_wakil' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'visi' => array(
                                'type' => 'LONGTEXT',
                        ),
                        'misi' => array(
                                'type' => 'LONGTEXT',
                        ),
                        'photo' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                ));
                $this->dbforge->add_key('calon_id', TRUE);
                $this->dbforge->create_table('calon');
        }

        public function down()
        {
                $this->dbforge->drop_table('calon');
        }
}