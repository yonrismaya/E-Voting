<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_pemilih extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'pemilih_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'login' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                                'null' => false,
                        ),
                        'nama' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'alamat' => array(
                                'type' => 'LONGTEXT',
                        ),
                        'jk' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        ),
                        'tgl_lahir' => array(
                                'type' => 'date',
                        ),
                        'umur' => array(
                                'type' => 'int',
                                'constraint' => 11,
                        ),
                        'status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'telepon' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        ),
                ));
                $this->dbforge->add_key('pemilih_id', TRUE);
                $this->dbforge->create_table('pemilih');
        }

        public function down()
        {
                $this->dbforge->drop_table('pemilih');
        }
}