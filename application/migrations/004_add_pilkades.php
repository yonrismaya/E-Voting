<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_pilkades extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'pilkades_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'pilkades_tahun' => array(
                                'type' => 'year',
                                'constraint' => '4',
                        ),
                        'pilkades_tanggal_mulai' => array(
                                'type' => 'date',
                        ),
                        'pilkades_tanggal_selesai' => array(
                                'type' => 'date',
                        ),
                        'pilkades_keterangan' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                ));
                $this->dbforge->add_key('pilkades_id', TRUE);
                $this->dbforge->create_table('pilkades');
        }

        public function down()
        {
                $this->dbforge->drop_table('pilkades');
        }
}